import transformers
import torch
from transformers import AutoModel, AutoTokenizer

cos = torch.nn.CosineSimilarity(dim=0, eps=1e-6)

MODEL_NAME = "skimai/electra-small-spanish" # Un modelo más chiquito

# We need to create the model and tokenizer
model = AutoModel.from_pretrained(MODEL_NAME,output_hidden_states=True)
tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)


def analysis(texto):
    input = tokenizer(texto, return_tensors="pt")
    return input

def comparar(texto1,texto2):
    input1 = tokenizer(texto1, return_tensors="pt")
    hidden=model(**input1)[1]
    X=hidden[-1][0]
    frase1=X[0]
    input2 = tokenizer(texto2, return_tensors="pt")
    hidden=model(**input2)[1]
    X=hidden[-1][0]
    frase2=X[0]

    return cos(frase1,frase2).tolist()

