from pydantic import BaseModel
from fastapi import FastAPI
from .ml import model, tokenizer, analysis, comparar

class Argumentos_analisis(BaseModel):
    texto: str = "Estoy muy feliz"

class Argumentos_analisis2(BaseModel):
    texto1: str = "Estoy muy feliz"
    texto2: str = "Estoy muy triste"

def create_api():
    app = FastAPI()

    @app.get('/')
    def main():
        return {"msg": "Hola soy la api ejemplo y estoy viva"}

    @app.post('/analisis')
    def json_analisis(args: Argumentos_analisis):
        # Desempacar
        texto=args.texto
        # Analisis
        respuesta=analysis(texto)
        # Empaquetar respuesta
        return { "texto_original": texto,
                "ids": respuesta["input_ids"][0].tolist()}


    @app.post('/comparar')
    def json_analisis(args: Argumentos_analisis2):
        # Desempacar
        texto1=args.texto1
        texto2=args.texto2
        # Analisis
        respuesta=comparar(texto1,texto2)
        # Empaquetar respuesta
        return { "texto_original_q": texto1,
                "terxto_original_2": texto2,
                "similar":str(respuesta)}



    return app

app=create_api()
